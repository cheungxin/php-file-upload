# PHP + Ajax file upload interface

# # # #
PHP + Ajax file upload interface, Ajax file upload (limit within 5M), PHP returns the corresponding interface

#### Software Architecture
Software architecture description JQ PHP


#### Installation Tutorial

1. Introduce the JQ
2. Change the Ajax upload port to the required interface

#### Instructions

1. The requested path for uploading files must be absolute
2. You can modify the interface limit as required
3. Create an “upload” empty folder