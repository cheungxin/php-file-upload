<?php 
header("Content-Type: text/html;charset=utf-8");
header('content-type:application:json;charset=utf8');
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:POST');
header('Access-Control-Allow-Headers:x-requested-with,content-type'); 
// 允许上传的图片后缀
$allowedExts = array("gif", "jpeg", "jpg", "png","pdf","xlsx","xls","doc","docx");
$temp = explode(".", $_FILES["file"]["name"]);
$nowdate =  date("Ymdhisa");
$newname =  "gz_zw_".$nowdate;
// echo $_FILES["file"]["size"];
// echo  $newname; 
$extension =  end($temp);     // 获取文件后缀名
$list=array();
$oldname =  $_FILES["file"]["name"]; 
$size = ($_FILES["file"]["size"] / 1024)."kb";
$msg = null; 
 
if ( ($_FILES["file"]["size"] <= 5120000)  &&  in_array($extension, $allowedExts)) 
{
	if ($_FILES["file"]["error"] > 0)
	{
		$list =array( "msg"=>"文件错误：: ".$_FILES["file"]["error"]);
	}
	else
	{  
		$tmp_name = $_FILES["file"]["tmp_name"];
		if (file_exists("upload/" . $_FILES["file"]["name"]))
		{ 
			$list =array(
			"status"=>"3" ,
			"msg"=>$_FILES["file"]["name"]." 文件已经存在 ",
			"type" => $_FILES["file"]["type"],
			"url" =>$_SERVER['HTTP_REFERER'] ."upload/" . $_FILES["file"]["name"],
			"time"=>date("Ymd h:i:s")
			);
		}
		else
		{ 
			move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $_FILES["file"]["name"]);
			$list =array(
			"status"=>"1" ,"msg"=>"上传成功！",
			"type" => $_FILES["file"]["type"],
			"url" => $_SERVER['HTTP_REFERER'] ."upload/" . $_FILES["file"]["name"],
			"time"=>date("Ymd h:i:s")
			);
		} 
	}
}
else
{ 
	$list =array("status"=>"2" ,"msg"=>"文件过大或文件不存在:".$size,"extension"=>$extension,"time"=>date("Ymd h:i:s"));
}
   echo json_encode($list); 
?>